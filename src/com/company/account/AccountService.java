package com.company.account;

import java.util.Scanner;

public class AccountService {
	Account[] baza = new Account[2];

	static private Account account;

	public static Account getAccount() {
		return account;
	}

	public AccountService() {
		baza[0] = new Account("user1", "password", true);
		baza[1] = new Account("user2", "password2", false);

	}

	public void login() throws Exception {
		Scanner scanner = new Scanner(System.in);
		String userName;
		String password;

		System.out.println("podaj nazwe uzytkownika");
		userName = scanner.nextLine();

		System.out.println("Podaj haslo");
		password = scanner.nextLine();

		for (int i = 0; i < baza.length; i++) {
			if (userName.equals(baza[i].getUserName())) {
				if (password.equals(baza[i].getPassword())) {
					AccountService.account = baza[i];
					return;
				}
			}
		}

		throw new Exception("Bledny login/haslo");
	}

}
