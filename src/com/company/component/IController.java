package com.company.component;

/**
 * Created by lukaszlesniewski on 12.12.2015.
 */
public interface IController {

    /**
     * ...
     */
    void create();
    void read();
    void update();
    void delete();

}
