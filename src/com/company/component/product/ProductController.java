package com.company.component.product;

import com.company.account.AccountService;
import com.company.component.IController;

public class ProductController implements IController {

    public void create() {

        if (!AccountService.getAccount().getIsKlient())
            System.out.println("tworzenie produktu");
    }

    public void read() {

        System.out.println("czytanie produktu");
    }

    public void update() {
        if (!AccountService.getAccount().getIsKlient())
            System.out.println("update produktu");
    }

    public void delete() {
        if (!AccountService.getAccount().getIsKlient())
            System.out.println("kasowanie produktu");
    }

}
