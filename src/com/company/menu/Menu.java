package com.company.menu;

import java.util.Scanner;

public abstract class Menu {

    public abstract String[] getOpcje();
    public abstract void executeComponent(int id) throws Exception ;

    public void print() {
        String[] opcje = getOpcje();
        try {
            Scanner scanner  = new Scanner(System.in);
            for (int i = 0; i < opcje.length; i++) {
                System.out.format("[%s]: %s\n", i + 1, opcje[i]);
            }
            String wybor;
            wybor = scanner.nextLine();

            executeComponent(wybor.charAt(0));
        } catch (Exception e) {
            print();
        }


    }

}
