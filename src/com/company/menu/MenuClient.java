package com.company.menu;

import com.company.component.cart.CartContoller;
import com.company.component.product.ProductController;
import com.company.menu.Menu;

public class MenuClient extends Menu {

    @Override
    public String[] getOpcje() {
        return new String[] {"Produkty", "Koszyk"};
    }

    @Override
    public void executeComponent(int id) throws Exception {

        if (id == '1') {
            ProductController productController = new ProductController();
        } else if(id == '2') {
            CartContoller cartContoller = new CartContoller();
        } else {
            throw new Exception("Nie ma takiej opcji");
        }

    }

}
