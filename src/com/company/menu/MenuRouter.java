package com.company.menu;

import com.company.account.AccountService;

public class MenuRouter {

    public static void route() {
        Menu menu;
        if (AccountService.getAccount().getIsKlient()) {
            menu = new MenuClient();
        } else {
            menu = new MenuEmp();
        }
        menu.print();
    }

}
